package id.ac.ui.cs.advprog.tutorial1.strategy;

public class Squeak implements QuackBehavior{

    @java.lang.Override
    public void quack() {
        System.out.println("Squeak!");
    }
}
