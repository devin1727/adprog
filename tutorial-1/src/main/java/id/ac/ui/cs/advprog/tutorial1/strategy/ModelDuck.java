package id.ac.ui.cs.advprog.tutorial1.strategy;

public class ModelDuck extends Duck{
    public ModelDuck(){
        FlyBehavior fb = new FlyWithWings();
        QuackBehavior qb = new Quack();
        setFlyBehavior(fb);
        setQuackBehavior(qb);
    }
    @Override
    public void display(){
        System.out.println("I'm a Model Duck");
    }

    @Override
    public void swim(){
        System.out.println("I can't swim");
    }
}
