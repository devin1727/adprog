package id.ac.ui.cs.advprog.tutorial1.strategy;

public class MallardDuck extends Duck {
    public MallardDuck(){
        FlyBehavior fb = new FlyWithWings();
        QuackBehavior qb = new Quack();
        setFlyBehavior(fb);
        setQuackBehavior(qb);
    }
    @Override
    public void display(){
        System.out.println("I'm a Mallard Duck");
    }
    @Override
    public void swim(){
        System.out.println("I can swim");
    }

}
