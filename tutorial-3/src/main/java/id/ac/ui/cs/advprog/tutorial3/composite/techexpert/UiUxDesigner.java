package id.ac.ui.cs.advprog.tutorial3.composite.techexpert;

import id.ac.ui.cs.advprog.tutorial3.composite.Employees;
import jdk.nashorn.internal.runtime.regexp.joni.exception.ValueException;
import java.lang.RuntimeException;

public class UiUxDesigner extends Employees {

    public UiUxDesigner(String name, double salary){
        if (salary < 90000.00){
            throw new IllegalArgumentException();
        }

        super.name = name;
        super.salary = salary;
        role = "UI/UX Designer";

    }

    @Override
    public double getSalary() {
        return salary;
    }

    @Override
    public String getRole() {
        return role;
    }
}